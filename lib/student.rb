class Student
  attr_accessor :first_name
  attr_accessor :last_name
  attr_accessor :courses

  def initialize(first_name, last_name)
    @first_name = first_name
    @last_name = last_name
    @courses = []
  end

  def courses
    return @courses
  end

  def name
    return @first_name + " " + @last_name
  end

  def enroll(course)
    courses.each{|i| raise "Course conflict!" if i.conflicts_with?(course)}
    if !@courses.include?(course)
      @courses << course
      course.students << self
    end
  end

  def course_load
    hash = Hash.new(0)
    @courses.each do |i|
      hash[i.department] += i.credits
    end
    hash
  end


end
